<?php

  include("./constants.php");
  
  // Create connection
  $conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 

  $sql = "SELECT * FROM ".TBL_SHOPPING_LISTS;
  $result = $conn->query($sql);
  $rows = array();
    while($r = $result->fetch_assoc()) {
      $rows['lists'][] = $r;
    }

  echo json_encode($rows);

?>
