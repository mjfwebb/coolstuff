// Load categories
function loadCatgories() {
  fetch('./getCategories.php')
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    addCategories(data);
  }); 
}

loadCatgories(); 

function addCategories(data) {
  const categorySelect = document.querySelector('#form select[name="category"]');
  categorySelect.innerHTML = "";
  for(i = 0; i < data.categories.length; i += 1) {
    const option = document.createElement("option");
    option.value = data.categories[i].id;
    option.innerText = data.categories[i].name;
    categorySelect.appendChild(option);
  } 
}

function loadShoppingLists() {
  fetch('./getShoppingLists.php')
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    displayShoppingLists(data)
  });  
}

loadShoppingLists();

function displayShoppingLists(data) {
  const shoppingListSelect = document.querySelector('#form select[name="shopping-list"]');
  const shoppingLists = document.getElementById('shopping-lists');
  shoppingLists.innerHTML = "";
  shoppingListSelect.innerHTML = "";
  for(i = 0; i < data.lists.length; i += 1) {
    //Add shopping lists to select in form.
    const option = document.createElement("option");
    option.value = data.lists[i].id;
    option.innerText = data.lists[i].name;
    shoppingListSelect.appendChild(option);

    //Add shopping lists view
    const div  = document.createElement("div");
    const p = document.createElement("p");
    p.innerHTML = data.lists[i].name;
    div.appendChild(p)
    div.id = "sl" + data.lists[i].id;

    //Create table
    const table = document.createElement("table");
    //Add table header
    addTableHeader(table)

    div.appendChild(table)
    shoppingLists.append(div);
    loadItems();
  }

  //Add "New..." shopping list item in form to create a new shopping list
  const option = document.createElement("option");
  option.value = "new";
  option.innerText = "New...";
  shoppingListSelect.appendChild(option);
}

// Load previously entered items.
function loadItems() {
  fetch('./getItems.php')
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    displayItems(data);
  });
}

function displayItems(data) {
  for(i = 0; i < data.items.length; i += 1) {
    const listId = data.items[i].list;
    const shoppingList = document.querySelector("#sl" + listId + " > table");
    // console.log("shoppingList", shoppingList)
    if(shoppingList) {
      const tr = document.createElement("tr");
      const tdName  = document.createElement("td");
      tdName.innerHTML = data.items[i].name;
      tr.appendChild(tdName);
      const tdQuantity = document.createElement("td");
      tdQuantity.innerHTML = data.items[i].quantity;
      tr.appendChild(tdQuantity);
      const tdCategory = document.createElement("td");
      tdCategory.innerHTML = data.items[i].category;
      tr.appendChild(tdCategory);
      const tdEdit = document.createElement("td");
      tdEdit.innerHTML = "<button id=\"edit-"+data.items[i].id+"\">Edit</button>";
      tr.appendChild(tdEdit); 
      const tdDelete = document.createElement("td");
      tdDelete.innerHTML = "<button id=\"delete-"+data.items[i].id+"\">Delete</button>";
      tr.appendChild(tdDelete); 
      shoppingList.appendChild(tr);
    }
  }
}

document.getElementById("form").addEventListener("submit", (event) => {
  event.preventDefault();
  // Check values aren't empty
  const name = document.querySelector('#form input[name="name"]').value
  const quantity = document.querySelector('#form input[name="quantity"]').value
  const category = document.querySelector('#form select[name="category"]').value
  const list = document.querySelector('#form select[name="shopping-list"]').value;
  if(!name || !quantity || !category || !list) {
    alert("Please enter values into all fields.");
  } else {
    (async () => {
      const rawResponse = await fetch('./addItem.php', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({name: name, quantity: quantity, category: category, list: list})
      });
      const content = await rawResponse.json();
    
      if(content === true) {
        loadShoppingLists();
      }
    })();
  }
})

function addTableHeader(table) {
  //Add table header
  const tr = document.createElement("tr");
  const thName = document.createElement("th");
  thName.innerHTML = "Name";
  tr.appendChild(thName);
  const thQuantity = document.createElement("th");
  thQuantity.innerHTML = "Quantity";
  tr.appendChild(thQuantity);
  const thCategory = document.createElement("th");
  thCategory.innerHTML = "Category";
  tr.appendChild(thCategory); 
  const thEdit = document.createElement("th");
  thEdit.innerHTML = "Edit";
  tr.appendChild(thEdit); 
  const thDelete = document.createElement("th");
  thDelete.innerHTML = "Delete";
  tr.appendChild(thDelete); 
  table.appendChild(tr);
}
