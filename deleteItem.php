<?php
  include("./constants.php");

  $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';

  if ($contentType === "application/json") {
    //Receive the RAW post data.
    $content = trim(file_get_contents("php://input"));

    $decoded = json_decode($content, true);

    //If json_decode failed, the JSON is invalid.
    if(is_array($decoded)) {
      // Create connection
      $conn = new mysqli(DB_SERVER, DB_USER, DB_PASS, DB_NAME);

      // Check connection
      if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
      }

      $itemId = $decoded["itemId"];

      $sql = "DELETE FROM ".TBL_ITEMS." WHERE id=$itemId";

      if($result === true) {
        echo json_encode($result);
      } else {
        echo json_encode("Error deleting item: " . $conn->error);
      }
    } else {
      echo "Error";
      // Send error back to user.
    }
  }

?>
